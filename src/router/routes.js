
const routes = [
  {
    path: '/',
    component: () => import('layouts/MyLayout.vue'),
    children: [
      { path: '/firestore', component: () => import('../pages/TestVueComputedFirestore.vue') },
      { path: '/splitImmediate', component: () => import('../pages/TestSplitImmediate.vue') },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
