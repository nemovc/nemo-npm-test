// Copy this file to env.js, filling out FIREBASE_CONFIGS

const FIREBASE_CONFIG = {
  apiKey: '',
  authDomain: '',
  databaseURL: '',
  projectId: '',
  storageBucket: '',
  messagingSenderId: '',
  appId: '',
};

module.exports = {
  FIREBASE_CONFIG,
};
