import firebase from 'firebase/app';
import 'firebase/auth';
import 'firebase/firestore';
// import VueComputedFirestore from '../plugins/vue-computed-firestore';
// const VueComputedFirestore = require('../plugins/vue-computed-firestore');
// const VueComputedFirestore = require('vue-computed-firestore');
// import * as VueComputedFirestore from 'vue-computed-firestore';
import VueComputedFirestore from 'vue-computed-firestore';

console.log(VueComputedFirestore);

const { FIREBASE_CONFIG } = require('../../env');

export default ({ Vue }) => {
  const app = firebase.initializeApp(FIREBASE_CONFIG);
  Vue.prototype.$appFirestore = app.firestore();
  Vue.prototype.$firebase = app;
  Vue.use(VueComputedFirestore, {
    firebase: Vue.prototype.$firebase,
    handlers: { error: e => console.log(e) },
  });
};
